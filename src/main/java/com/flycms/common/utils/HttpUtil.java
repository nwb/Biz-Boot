package com.flycms.common.utils;

import com.flycms.common.utils.result.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 通用拦截器
 * @email 79678111@qq.com
 * @Date: 14:35 2019/11/21
 */
public class HttpUtil {

  public static boolean isApiRequest(HttpServletRequest request) {
    return request.getHeader("Accept") == null || !request.getHeader("Accept").contains("text/html");
  }

  // 根据请求接收的类型定义不同的响应方式
  // 判断请求对象request里的header里accept字段接收类型
  // 如果是 text/html 则响应一段js，这里要将response对象的响应内容类型也设置成 text/javascript
  // 如果是 application/json 则响应一串json，response 对象的响应内容类型要设置成 application/json
  // 因为响应内容描述是中文，所以都要带上 ;charset=utf-8 否则会有乱码
  // 写注释真累费劲。。
  public static void responseWrite(HttpServletRequest request, HttpServletResponse response) throws IOException {
    if (!HttpUtil.isApiRequest(request)) {
      response.setContentType("text/html;charset=utf-8");
      response.getWriter().write("<script>alert('请先登录!');window.history.go(-1);</script>");
    } else /*if (accept.contains("application/json"))*/ {
      response.setContentType("application/json;charset=utf-8");
      response.getWriter().write(JsonUtil.objectToJson(Result.failure("请先登录")));
    }
  }
}
