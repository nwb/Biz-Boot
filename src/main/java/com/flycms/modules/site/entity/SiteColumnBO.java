package com.flycms.modules.site.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 网站栏目
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:18 2019/8/17
 */
@Setter
@Getter
public class SiteColumnBO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	@NotNull(message = "上级分类参数不能为空")
	@Pattern(regexp = "[0-9]\\d*", message = "上级分类参数错误")
	private String parentId;

	@NotNull(message = "网站id参数不能为空")
	@Pattern(regexp = "[1-9]\\d*", message = "网站id参数错误")
	private String siteId;

	@NotNull(message = "栏目模型参数不能为空")
	private String channel;  //对应模型表fly_system_module里的id，每个id对应一个模型

	@NotNull(message = "栏目名称不能为空")
	@Size(min=2,max = 60,message = "请输入栏目名称，2-60个字符")
	private String columnTitle;
	private String columnUrl;

	@NotNull(message = "超链接是否新窗口参数不能为空")
	@Max(value = 1, message = "勿传输新窗口传递非法字符")
	@Min(value = 0, message = "勿传输新窗口传递非法字符")
	private Integer newWindow;

	@NotNull(message = "是否隐藏栏目设置参数不能为空")
	@Max(value = 1, message = "勿传输新窗口传递非法字符")
	@Min(value = 0, message = "勿传输新窗口传递非法字符")
	private Integer columnHidden;	//栏目是否隐藏

	@NotNull(message = "栏目属性设置参数不能为空")
	@Range(min=1,max=4,message = "请选择正确的栏目属性，勿传输非法字符")
	private Integer columnType;      //栏目属性，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
	private String tempindex;
	private String templist;
	private String tempcontent;
	private String tempalone;
	private String title;
	private String keywords;
	private String description;
	private String content;
}