package com.flycms.modules.site.controller;


import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.PageAssert;
import com.flycms.common.utils.result.Result;
import com.flycms.common.validator.Sort;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.entity.Site;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.site.service.SiteService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 网站运营后台官网管理页面
 *
 * @author 孙开飞
 */
@Controller
@RequestMapping("/admin/system_site")
public class SiteSystemController extends BaseController {

    @Autowired
    private SiteService siteService;
    @Autowired
    private SiteCompanyService siteCompanyService;

    //系统官网首页
    @GetMapping("/index{url.suffix}")
    public String indexSite(Model model)
    {
        Site site=siteService.findById(Long.parseLong(systemService.findByKeyCode("default_site")));
        if(site == null){
            return "redirect:/404.do";
        }
        model.addAttribute("site",site);
        return "system/admin/system/site/index";
    }

    /**
     * 独立网站欢迎页面
     */
    @RequestMapping("/welcome${url.suffix}")
    public String welcome(Model model){
        return "system/admin/system/site/welcome";
    }

    //查询网站二级域名是否被占用
    @PostMapping("/queryDomain{url.suffix}")
    @ResponseBody
    public Object queryDomain(@RequestParam(value = "domain", required = false) String domain,
                              @RequestParam(value = "siteId", required = false) String siteId)
    {
        Long id = null;
        if(!StringUtils.isEmpty(siteId)){
            if (NumberUtils.isNumber(siteId)) {
                id = Long.parseLong(siteId);
                PageAssert.notTrue(!siteCompanyService.checkSiteCompany(id), "您不是网站管理员，请勿非法操作");
            }
        }

        if(siteService.holdSiteByDomain(domain)){
            return Result.failure("请修改当前网站二级域名，该域名已被平台保留");
        }
        if(!siteService.checkSiteByDomain(domain,id)){
            return Result.success("该二级域名可以使用");
        }
        return Result.failure("该二级域名已被占用");
    }

    //编辑网站基本信息
    @GetMapping("/edit{url.suffix}")
    public String edit(Model model)
    {
        Site site = siteService.findById(Long.parseLong(systemService.findByKeyCode("default_site")));
        model.addAttribute("site",site);
        return "system/admin/system/site/edit";
    }

    //保存网站信息
    @PostMapping("/edit{url.suffix}")
    @ResponseBody
    public Object edit(Site site)
    {
        return siteService.updateAdminSiteById(site);
    }

}