package com.flycms.modules.site.service.impl;

import com.flycms.common.exception.ApiAssert;
import com.flycms.common.pager.Pager;
import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.news.entity.News;
import com.flycms.modules.news.service.NewsService;
import com.flycms.modules.product.entity.Product;
import com.flycms.modules.product.service.ProductService;
import com.flycms.modules.site.dao.ContentDao;
import com.flycms.modules.site.entity.Content;
import com.flycms.modules.site.entity.ContentVO;
import com.flycms.modules.site.entity.SiteColumn;
import com.flycms.modules.site.service.ContentService;
import com.flycms.modules.site.service.SiteColumnService;
import com.flycms.modules.site.service.SiteCompanyService;
import com.flycms.modules.system.entity.SystemModule;
import com.flycms.modules.system.service.SystemModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 0:05 2019/10/18
 */
@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    private ContentDao contentDao;
    @Autowired
    private SiteCompanyService siteCompanyService;
    @Autowired
    private SiteColumnService siteColumnService;
    @Autowired
    private SystemModuleService systemModuleService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private ProductService productService;

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    public Object selectContentAllListPager(Content content, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(content.getTitle())) {
            whereStr.append(" and title like concat('%',#{entity.title},'%')");
        }
        if (!StringUtils.isEmpty(content.getColumnId())) {
            whereStr.append(" and column_id = #{entity.columnId}");
        }
        whereStr.append(" and deleted = 0");
        whereStr.append(" and site_id = #{entity.siteId}");
        Pager<Product> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        Product entity = new Product();
        entity.setTitle(content.getTitle());
        entity.setSiteId(content.getSiteId());
        entity.setColumnId(content.getColumnId());
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        List<Content> sitelsit = contentDao.queryContentList(pager);
        List<ContentVO> volsit = new ArrayList<ContentVO>();
        sitelsit.forEach(bean -> {
            ContentVO vo=new ContentVO();
            vo.setId(bean.getId().toString());
            vo.setTitle(bean.getTitle());
            if(bean.getMold()==1){
                vo.setModuleType("news");
            }else if(bean.getMold()==2){
                vo.setModuleType("product");
            }
            vo.setCreateTime(bean.getCreateTime());
            volsit.add(vo);
        });
        return LayResult.success(0, "true", contentDao.queryContentTotal(pager), volsit);
    }

    /**
     * 按分类id查询该分类内容翻页列表
     *
     * @param columnId
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectContentListPager(String siteId,String columnId,String title, Integer page, Integer pageSize, String sort, String order) {
        if (!StringUtils.isEmpty(columnId) && Long.parseLong(columnId) > 0) {
            SiteColumn column = siteColumnService.findById(Long.parseLong(columnId));
            if (column != null) {
                ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(column.getSiteId()), "您不是网站管理员，请勿非法操作");
                SystemModule module = systemModuleService.findById(column.getChannel());
                if (module != null) {
                    if ("news".equals(module.getModuleType())) {
                        News news = new News();
                        news.setSiteId(column.getSiteId());
                        news.setColumnId(Long.parseLong(columnId));
                        news.setTitle(title);
                        return newsService.selectNewsListLayPager(news, page, pageSize, sort, order);
                    } else if ("product".equals(module.getModuleType())) {
                        Product product = new Product();
                        product.setSiteId(column.getSiteId());
                        product.setColumnId(Long.parseLong(columnId));
                        product.setTitle(title);
                        return productService.selectProductListPager(product, page, pageSize, sort, order);
                    }
                }
            }
        }else{
            Content content = new Content();
            content.setSiteId(Long.parseLong(siteId));
            content.setTitle(title);
            return this.selectContentAllListPager(content, page, pageSize, sort, order);
        }
        return Result.failure("未知错误");
    }
}
