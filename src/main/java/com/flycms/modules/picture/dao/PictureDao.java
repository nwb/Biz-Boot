package com.flycms.modules.picture.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.picture.entity.Picture;
import com.flycms.modules.picture.entity.PictureCompany;
import com.flycms.modules.picture.entity.PictureInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface PictureDao extends BaseDao<Picture> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    public int savePictureCompany(PictureCompany pictureCompany);

    /**
     * 添加信息和图片库关联信息
     *
     * @param pictureInfo
     * @return
     */
    public int savePictureInfo(PictureInfo pictureInfo);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 按id删除图片主表信息
     *
     * @param id
     * @return
     */
    public int deleteById(@Param("id") Long id);

    /**
     * 按图片库主表id和企业id删除关联信息
     *
     * @param pictureId
     * @param companyId
     * @return
     */
    public int deletePictureCompanyById(@Param("pictureId") Long pictureId,@Param("companyId") Long companyId);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 更新统计当前图片使用数量
     *
     * @param countInfo
     * @param id
     * @return
     */
    public int updatePictureInfoCount(@Param("countInfo") Integer countInfo,@Param("id") Long id);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 按企业id和图片唯一指纹查询是否存在
     *
     * @param companyId
     * @param signature
     * @return
     */
    public int checkPictureBySignature(@Param("companyId") Long companyId, @Param("signature") String signature);

    /**
     * 按信息id和图片id查询是否存在
     *
     * @param pictureId
     *         图片id
     * @param infoId
     *         信息id
     * @return
     */
    public int checkPictureByInfoId(@Param("pictureId") Long pictureId, @Param("infoId") Long infoId);

    /**
     * 按图片指纹和企业id查询图片被使用的数量
     *
     * @param companyId
     * @param signature
     * @return
     */
    public int queryPictureByInfoTotal(@Param("companyId") Long companyId, @Param("signature") String signature);

    /**
     * 按企业id和图片唯一指纹查询图片信息
     *
     * @param companyId
     * @param signature
     * @return
     */
    public Picture findPictureBySignature(@Param("companyId") Long companyId, @Param("signature") String signature);
}
