package com.flycms.modules.system.service;

import com.flycms.common.utils.result.Result;
import com.flycms.modules.system.entity.SystemModule;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 1:53 2019/8/25
 */
public interface SystemModuleService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 添加模块信息
     *
     * @param systemModule
     * @return
     */
    public Result addModule(SystemModule systemModule);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 按id查询网站信息
     *
     * @param id
     * @return
     */
    public SystemModule findById(Long id);

    /**
     * 查询用户所有网站列表
     *
     * @return
     */
    public List<SystemModule> selectModuleList();

    /**
     * 查询模型翻页列表
     *
     * @param module
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectModulePager(SystemModule module, Integer page, Integer pageSize, String sort, String order);
}
