package com.flycms.modules.system.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.service.SiteService;
import com.flycms.modules.user.entity.AdminMenu;
import com.flycms.modules.user.service.AdminMenuService;
import com.flycms.modules.system.service.ConfigureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:50 2019/8/19
 */
@Controller
public class IndexAdminController extends BaseController {
    @Autowired
    private AdminMenuService adminMenuService;
    @Autowired
    private ConfigureService systemService;
    @Autowired
    private SiteService siteService;

    // 管理员首页
    @GetMapping("/admin/index{url.suffix}")
    public String adminIndex(Model model) {
        // 根据用户id取出菜单
        List<AdminMenu> menus = adminMenuService.selectMenusByUser(ShiroUtils.getLoginUser().getId());
        model.addAttribute("menus", menus);
        model.addAttribute("user", getUser());
        return "system/admin/index";
    }

    /**
     * 登陆成功之后进入的页面，欢迎页面
     */
    @RequestMapping("/admin/welcome${url.suffix}")
    public String welcome(HttpServletRequest request, Model model){
        return "system/admin/welcome";
    }
}
