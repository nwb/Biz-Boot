package com.flycms.modules.news.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 */
@Setter
@Getter
public class NewsData implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String text;
	private String extend;	//扩展，以json形式存在

}