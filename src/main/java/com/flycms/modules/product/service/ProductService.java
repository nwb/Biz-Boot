package com.flycms.modules.product.service;

import com.flycms.modules.product.entity.Product;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 17:07 2019/8/14
 */
public interface ProductService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 企业用户发布产品信息
     *
     * @param product
     * @param companyId
     * @param typeId
     * @return
     */
    public Object addUserProduct(Product product, Long companyId, Integer typeId);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除产品信息
     *
     * @param id
     * @return
     */
    public int deleteById(Long id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////



    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 产品翻页列表
     *
     * @param product
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Object selectProductListPager(Product product, Integer page, Integer pageSize, String sort, String order);
}
