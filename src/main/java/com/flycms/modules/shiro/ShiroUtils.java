package com.flycms.modules.shiro;

import com.flycms.common.utils.StringUtils;
import com.flycms.common.utils.bean.BeanUtils;
import com.flycms.modules.user.entity.Admin;
import com.flycms.modules.user.entity.LoginUser;
import com.flycms.modules.user.entity.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 9:58 2019/8/19
 */
public class ShiroUtils {
    /**  加密算法 */
    public final static String hashAlgorithmName = "SHA-256";
    /**  循环次数 */
    public final static int hashIterations = 16;

    /**  查询用户登录信息 **/
    public static LoginUser getLoginUser()
    {
        LoginUser user = null;
        Object obj = getSubject().getPrincipal();
        if (StringUtils.isNotNull(obj))
        {
            user = new LoginUser();
            BeanUtils.copyBeanProp(user, obj);
        }
        return user;
    }

    /**  查询管理员登录信息 **/
    public static LoginUser getLoginAdmin()
    {
        LoginUser user = null;
        Object obj = getSubject().getPrincipal();
        if (StringUtils.isNotNull(obj))
        {
            user = new LoginUser();
            BeanUtils.copyBeanProp(user, obj);
        }
        return user;
    }

    /** 更新用户登录信息 **/
    public static void setLoginUser(LoginUser loginUser)
    {
        Subject subject = getSubject();
        PrincipalCollection principalCollection = subject.getPrincipals();
        String realmName = principalCollection.getRealmNames().iterator().next();
        PrincipalCollection newPrincipalCollection = new SimplePrincipalCollection(loginUser, realmName);
        // 重新加载Principal
        subject.runAs(newPrincipalCollection);
    }

    public static String sha256(String password, String salt) {
        return new SimpleHash(hashAlgorithmName, password, salt, hashIterations).toString();
    }

    public static Session getSession() {
        return SecurityUtils.getSubject().getSession();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static User getUserEntity() {
        return (User)SecurityUtils.getSubject().getPrincipal();
    }

    public static Long getUserId() {
        return getUserEntity().getId();
    }

    public static void setSessionAttribute(Object key, Object value) {
        getSession().setAttribute(key, value);
    }

    public static Object getSessionAttribute(Object key) {
        return getSession().getAttribute(key);
    }

    public static boolean isLogin() {
        return SecurityUtils.getSubject().getPrincipal() != null;
    }

    public static void logout() {
        SecurityUtils.getSubject().logout();
    }

    public static String getKaptcha(String key) throws IllegalAccessException {
        Object kaptcha = getSessionAttribute(key);
        if(kaptcha == null){
            throw new IllegalAccessException("验证码已失效");
        }
        getSession().removeAttribute(key);
        return kaptcha.toString();
    }
}
