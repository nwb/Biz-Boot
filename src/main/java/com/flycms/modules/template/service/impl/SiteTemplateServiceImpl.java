package com.flycms.modules.template.service.impl;

import com.flycms.common.pager.Pager;
import com.flycms.modules.template.dao.SiteTemplateOrderDao;
import com.flycms.modules.template.dao.SiteTemplateDao;
import com.flycms.modules.template.entity.SiteTemplateMerge;
import com.flycms.modules.template.entity.SiteTemplateOrder;
import com.flycms.modules.template.entity.SiteTemplatePage;
import com.flycms.modules.template.service.SiteTemplateService;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.StringWriter;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 企业模板服务类
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 15:58 2019/9/10
 */
@Service
public class SiteTemplateServiceImpl implements SiteTemplateService {
    private static final Logger log = LoggerFactory.getLogger(SiteTemplateServiceImpl.class);

    @Autowired
    private SiteTemplateDao siteTemplateDao;

    @Autowired
    private SiteTemplateOrderDao siteTemplateOrderDao;
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 按id删除网站模板详细页面信息
     *
     * @param id
     *         模板id
     * @return
     */
    public int deleteById(Long id){
        return siteTemplateDao.deleteById(id);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 得到当前请求需要渲染的模板相对路径
     *
     * @param template
     * @return
     */
    public String getTemplatePath(String template) {
        return template;
    }

    /**
     * 模板物理地址是否存在
     *
     * @param templatePath
     * @return
     */
    public Boolean isExist(String templatePath) {
        String themePath = "views/templates/"+templatePath + ".html";
        File file = new File(themePath);
        if (file.exists()) {
            //log.info("模板当前路径：" + themePath);
            return true;
        } else {
            log.error("模板不存在：" + file.getAbsolutePath());
            return false;
        }
    }

    /**
     * 获取企业网站模板
     *
     * @param siteId
     *         网站id
     * @param templateName
     *         模板文件名
     * @return
     */
    public String getSiteTemplate(Long siteId,String templateName) {
        SiteTemplateMerge merge = siteTemplateDao.findSiteTemplateBySiteId(siteId);
        if(merge==null){
            return this.getTemplatePath("FileNotFound");
        }
        String themePath = "/websiteTemplate"+merge.getTemplateCatalog()+"/"+merge.getTemplateName()+"/"+templateName;
        if (this.isExist(themePath)) {
            return this.getTemplatePath(themePath);
        }else{
            log.error(themePath+"模板文件不存在！！");
        }
        return this.getTemplatePath("FileNotFound");
    }

    //按id查询模板信息
    @Cacheable(key="'templatePage_'+#id",value="templatePage")
    public SiteTemplatePage findById(Long id) {
        return siteTemplateDao.findById(id);
    }

    /**
     * 按网站id、模板id、和模型id，查询模板页面列表
     *
     * *** 有两种可能：
     *     1、是查询当前网站其中一个分类可使用的所有模板，那么网站id、模板id、模型id、模板类型是必须传值，
     *     2、例如添加分类时选择分类使用的模型后，会根据网站id、模板id、模型id筛选出当前分类模型首页、列表、内容页可以使用的所有模板，模板类型可为null
     *
     * @param siteId
     *         网站id
     * @param templateId
     *         模板主表id
     * @param moduleId
     *         模型fly_system_module表id；对应的如新闻模块id，产品模块id
     * @param pageType
     *         模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
     * @return
     */
    public List<SiteTemplatePage> selectTemplatePageList(Long siteId, Long templateId, Long moduleId, Integer pageType) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(siteId)) {
            whereStr.append(" and site_id = #{entity.siteId}");
        }
        if (!StringUtils.isEmpty(templateId)) {
            whereStr.append(" and template_id = #{entity.templateId}");
        }
        if (!StringUtils.isEmpty(moduleId)) {
            whereStr.append(" and module_id = #{entity.moduleId}");
        }
        if (!StringUtils.isEmpty(pageType)) {
            whereStr.append(" and page_type = #{entity.pageType}");
        }
        whereStr.append(" and deleted = 0");

        Pager<SiteTemplatePage> pager = new Pager();
        //排序设置
        pager.addOrderProperty("", true,false);
        //使用limit进行查询翻页
        pager.addLimitProperty(false);
        //查询条件
        SiteTemplatePage page = new SiteTemplatePage();
        page.setSiteId(siteId);
        page.setTemplateId(templateId);
        page.setModuleId(moduleId);
        page.setPageType(pageType);
        pager.setEntity(page);
        pager.setWhereStr(whereStr.toString());
        return siteTemplateDao.queryList(pager);
    }

    //数据库模板读取通过freemarker解析输出整个完整页面
    public Object exportTemplate(String templatePage,Model model){
        try{
            StringWriter writer = new StringWriter();
            if(templatePage==null){
                writer.append("模板未设置");
                return writer.toString();
            }
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
            cfg.setDefaultEncoding("UTF-8");
            StringTemplateLoader stringLoader = new StringTemplateLoader();
            stringLoader.putTemplate("myTemplate",templatePage);
            cfg.setTemplateLoader(stringLoader);
            cfg.setSetting("classic_compatible","true");
            Template template = cfg.getTemplate("myTemplate","UTF-8");
            template.process(model, writer);
            return writer.toString();
        }catch(Exception e){
            log.debug("模板加载错误");
        }
        return null;
    }

    /**
     * 查询用户购买模板列表翻页
     *
     * @param siteId
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Pager<SiteTemplateOrder> selectTemplatePager(Long siteId, Integer page, Integer pageSize, String sort, String order) {
        StringBuffer whereStr = new StringBuffer(" 1 = 1");
        if (!StringUtils.isEmpty(siteId)) {
            whereStr.append(" and site_id = #{entity.siteId}");
        }
        Pager<SiteTemplateOrder> pager = new Pager(page, pageSize);
        //排序设置
        if (!StringUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        //查询条件
        SiteTemplateOrder entity = new SiteTemplateOrder();
        entity.setSiteId(siteId);
        pager.setEntity(entity);
        pager.setWhereStr(whereStr.toString());
        pager.setList(siteTemplateOrderDao.queryList(pager));
        pager.setTotal(siteTemplateOrderDao.queryTotal(pager));
        return pager;
    }
}
