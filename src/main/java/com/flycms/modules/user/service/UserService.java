package com.flycms.modules.user.service;

import com.flycms.common.utils.result.Result;
import com.flycms.modules.user.entity.RegisterUserVO;
import com.flycms.modules.user.entity.User;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 11:19 2019/8/18
 */
public interface UserService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 用户手机注册账号申请获取验证码
     *
     * @param phoneNumber
     *        手机号码
     * @return
     * @throws Exception
     */
    public Object regMobileCode(String phoneNumber)throws Exception;

    /**
     * 添加用户信息
     *
     * @param reg
     *         用户提交注册信息
     * @return
     */
    public Result addUser(RegisterUserVO reg);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    public Result updatePassword(String oldPassword, String password);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 排除当前用户id后查询用户名是否存在,如果userId设置为null则查全部的用户名
     *
     * @param userName
     *         用户登录手机号码
     * @param userId
     *         需要排除的user_id,可设置为null
     * @return
     */
    public boolean checkUserByUserName(String userName, Long userId);

    /**
     * 排除当前用户id后查询手机号码是否存在,如果userId设置为null则查全部的手机号码
     *
     * @param userMobile
     *         用户登录手机号码
     * @param userId
     *         需要排除的user_id,可设置为null
     * @return
     */
    public boolean checkUserByMobile(String userMobile, Long userId);

    //通过id查询用户信息
    public User findById(Long userId);

    //通过username查询用户信息
    public User findByUsername(String userName);

    /**
     * 翻页列表
     *
     * @param name
     *         根据分类名查询
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Object selectUserListPager(String name, Integer page, Integer limit, String sort, String order);
}
