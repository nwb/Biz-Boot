package com.flycms.modules.user.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.user.entity.AdminMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 菜单表 数据层
 * 
 * @author 孙开飞
 */
@Repository
public interface AdminMenuDao extends BaseDao<AdminMenu>{

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 删除菜单管理信息
     *
     * @param id 菜单ID
     * @return 结果
     */
    public int deleteMenuById(Long id);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改菜单信息
     *
     * @param adminMenu 菜单信息
     * @return 结果
     */
    public int updateMenu(AdminMenu adminMenu);

    /**
     * 修改菜单是否显示
     *
     * @param visible
     *         菜单状态（1显示 0隐藏）
     * @param updateUserId
     *         当前更新操作管理员id
     * @param id
     *         需要更新的信息id
     * @return
     *         返回成功条数
     */
    public int updateMenuVisible(@Param("visible") Boolean visible, @Param("updateUserId") Long updateUserId, @Param("id") Long id);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 校验菜单名称是否唯一
     *
     * @param menuName 菜单名称
     * @param parentId 父菜单ID
     * @param id 需要排除的id
     * @return 结果
     */
    public AdminMenu checkMenuNameUnique(@Param("menuName") String menuName, @Param("parentId") Long parentId, @Param("id") Long id);

    /**
     * 查询系统所有菜单（含按钮）
     * 
     * @return 菜单列表
     */
    public List<AdminMenu> selectMenuAll();

    /**
     * 根据用户ID查询菜单
     * 
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<AdminMenu> selectMenuAllByUserId(Long userId);

    /**
     * 查询系统正常显示菜单（不含按钮）
     * 
     * @return 菜单列表
     */
    public List<AdminMenu> selectMenuNormalAll();

    /**
     * 根据用户ID查询菜单
     * 
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<AdminMenu> selectMenusByUserId(Long userId);

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public List<String> selectPermsByUserId(Long userId);

    /**
     * 根据角色ID查询菜单
     * 
     * @param roleId 角色ID
     * @return 菜单列表
     */
    public List<String> selectMenuTree(Long roleId);

    /**
     * 查询系统菜单列表
     * 
     * @param adminMenu 菜单信息
     * @return 菜单列表
     */
    public List<AdminMenu> selectMenuList(AdminMenu adminMenu);

    /**
     * 查询系统菜单列表
     * 
     * @param adminMenu 菜单信息
     * @return 菜单列表
     */
    public List<AdminMenu> selectMenuListByUserId(AdminMenu adminMenu);

    /**
     * 按父ID查询菜单列表
     *
     * @param parentId 菜单父ID
     * @return 结果
     */
    public List<AdminMenu> selectMenuByParentId(Long parentId);

    /**
     * 查询菜单数量
     * 
     * @param parentId 菜单父ID
     * @return 结果
     */
    public int selectCountMenuByParentId(Long parentId);

}
